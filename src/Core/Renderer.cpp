#include "Renderer.h"

#include "SDL.h"
#include "SDL_ttf.h"

#include "Core/Log.h"

std::map<std::string_view, struct _TTF_Font*> Renderer::sFontCache;

Renderer::Renderer(SDL_Renderer* InRenderer) : SDLRender(InRenderer) {

}

Renderer::~Renderer() {
  if (SDLRender) {
    SDL_DestroyRenderer(SDLRender);
    SDLRender = nullptr;
  }
}

void Renderer::Present() {
  SDL_RenderPresent(SDLRender);
}

void Renderer::Clear(const FColor& Color) {
  SDL_SetRenderDrawColor(SDLRender, Color.R, Color.G, Color.B, Color.A);
  SDL_RenderClear(SDLRender);
}

void Renderer::SetDrawColor(const FColor& Color) {
  SDL_SetRenderDrawColor(SDLRender, Color.R, Color.G, Color.B, Color.A);
}

void Renderer::DrawRect(const FRect& Rect) {
  const SDL_FRect R{Rect.X, Rect.Y, Rect.Width, Rect.Height};
  SDL_RenderFillRectF(SDLRender, &R);
}

void Renderer::DrawRectOutline(const FRect& Rect) {
  const SDL_FRect R{Rect.X, Rect.Y, Rect.Width, Rect.Height};
  SDL_RenderDrawRectF(SDLRender, &R);
}

//https://gist.github.com/Gumichan01/332c26f6197a432db91cc4327fcabb1c
void Renderer::DrawCircle(const FPoint& Center, float Radius) {
  float OffsetX = 0.0f;
  float OffsetY = Radius;
  float RadiusError = Radius - 1.0f;

  while (OffsetY >= OffsetX)
  {
    SDL_RenderDrawLineF(SDLRender, Center.X - OffsetY, Center.Y + OffsetX, Center.X + OffsetY, Center.Y + OffsetX);
    SDL_RenderDrawLineF(SDLRender, Center.X - OffsetX, Center.Y + OffsetY, Center.X + OffsetX, Center.Y + OffsetY);
    SDL_RenderDrawLineF(SDLRender, Center.X - OffsetX, Center.Y - OffsetY, Center.X + OffsetX, Center.Y - OffsetY);
    SDL_RenderDrawLineF(SDLRender, Center.X - OffsetY, Center.Y - OffsetX, Center.X + OffsetY, Center.Y - OffsetX);

    if (RadiusError >= 2.0f * OffsetX)
    {
      RadiusError -= 2.0f * OffsetX + 1.0f;
      OffsetX += 1.0f;
    }
    else if (RadiusError < 2.0f * (Radius - OffsetY))
    {
      RadiusError += 2.0f * OffsetY - 1.0f;
      OffsetY -= 1.0f;
    }
    else
    {
      RadiusError += 2.0f * (OffsetY - OffsetX - 1.0f);
      OffsetY -= 1.0f;
      OffsetX += 1.0f;
    }
  }
}

bool Renderer::SetFont(const std::string_view& FontName, const int32_t FontSize) {
  auto CachedFont = sFontCache.find(FontName);
  _TTF_Font* Font = nullptr;
  if (CachedFont != sFontCache.end()) {
    Font = CachedFont->second;
  }
  else {
    Font = TTF_OpenFont(FontName.data(), FontSize);
    if (!Font) {
      Logger::error("TTF ERROR: %s", SDL_GetError());
      return false;
    }
    sFontCache.insert(std::make_pair(FontName, Font));
  }
  if (Font) {
    mCurrentFont = Font;
    TTF_SetFontSize(mCurrentFont, FontSize);
    return true;
  }
  return false;
}

void Renderer::DrawText(const std::string_view& Text, const FPoint& Position, EAlign HorAlign, EAlign VerAlign) {
  if (!mCurrentFont) {
    return;
  }
  FColor DrawColor;
  int32_t TextureWidth = 0;
  int32_t TextureHeight = 0;
  SDL_GetRenderDrawColor(SDLRender, &DrawColor.R, &DrawColor.G, &DrawColor.B, &DrawColor.A);
  SDL_Surface* Surface = TTF_RenderText_Blended(mCurrentFont, Text.data(), { DrawColor.R, DrawColor.G, DrawColor.B, DrawColor.A });
  SDL_Texture* Texture = SDL_CreateTextureFromSurface(SDLRender, Surface);

  SDL_QueryTexture(Texture, nullptr, nullptr, &TextureWidth, &TextureHeight);
  SDL_FRect DestRect = { Position.X, Position.Y, static_cast<float>(TextureWidth), static_cast<float>(TextureHeight) };
  switch (HorAlign) {
    case EAlign::Center: DestRect.x -= TextureWidth * 0.5f; break;
    case EAlign::Right: DestRect.x -= TextureWidth; break;
    default:break;
  }
  switch (VerAlign) {
    case EAlign::Center: DestRect.y -= TextureHeight * 0.5f; break;
    case EAlign::Right: DestRect.y -= TextureHeight; break;
    default:break;
  }
  SDL_RenderCopyExF(SDLRender, Texture, nullptr, &DestRect, 0.0f, nullptr, SDL_FLIP_NONE);

  SDL_FreeSurface(Surface);
  SDL_DestroyTexture(Texture);
}

void Renderer::ClearFontCache() {
  for (auto& [Key, Font] : sFontCache) {
    TTF_CloseFont(Font);
  }
  sFontCache.clear();
}