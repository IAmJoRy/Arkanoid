#include "ScreenManager.h"

#include "Core/Renderer.h"

ScreenManager::ScreenManager(std::unique_ptr<IScreenLayerFactory>&& InLayerFactory) : LayerFactory(std::move(InLayerFactory)) {
  RequestLayerId = LayerFactory->DefaulLayerId;
}

void ScreenManager::Update(float DeltaTime) {
  if (RequestLayerId != IScreenLayerFactory::InvalidLayerId) {
    HandleTransitions();
  }

  for (auto& Layer : ActiveLayers) {
    Layer->Update(DeltaTime);

    if (!Layer->ShouldUpdatePrevLayer())
      break;
  }
}

void ScreenManager::Draw(const std::shared_ptr<class Renderer>& Renderer) const {
  Renderer->Clear({});
  for (auto It = ActiveLayers.rbegin(); It != ActiveLayers.rend(); ++It) {
    auto NextIter = It + 1;
    if (NextIter == ActiveLayers.rend() || (*NextIter)->ShouldDrawPrevLayer()) {
      (*It)->Draw(Renderer);
    }
  }
  Renderer->Present();
}

void ScreenManager::KeyDownEvent(const FKeyboardEvent& Event) {
  for (auto& Layer : ActiveLayers) {
    if (Layer->HasBindings()) {
      if (const auto& KeyDelegate = Layer->GetBoundDelegate(Event.Key); KeyDelegate) {
        KeyDelegate(Event.State);
      }
    }

    if (!Layer->ShouldHandleInputsPrevLayer())
      break;
  }
}

void ScreenManager::KeyUpEvent(const FKeyboardEvent& Event) {
  for (auto& Layer : ActiveLayers) {
    if (Layer->HasBindings()) {
      if (const auto& KeyDelegate = Layer->GetBoundDelegate(Event.Key); KeyDelegate) {
        KeyDelegate(Event.State);
      }
    }

    if (!Layer->ShouldHandleInputsPrevLayer())
      break;
  }
}

void ScreenManager::WindowFocusInEvent() {
  if (ActiveLayers.size() > 0 && ActiveLayers.front()) {
    ActiveLayers.front()->WindowFocusInEvent();
  }
}

void ScreenManager::WindowFocusOutEvent() {
  if (ActiveLayers.size() > 0 && ActiveLayers.front()) {
    ActiveLayers.front()->WindowFocusOutEvent();
  }
}

void ScreenManager::RequestTransition(const std::shared_ptr<ScreenLayer>&Requester, int32_t LayerId) {
  if (ActiveLayers.size() > 0 && ActiveLayers.front()->GetId() == Requester->GetId()) {
    RequestLayerId = LayerId;
  }
}

void ScreenManager::HandleTransitions() {
  auto It = ActiveLayers.begin();
  for (; It != ActiveLayers.end(); ++It) {
    if (((*It)->GetId() == RequestLayerId)) {
      break;
    }
  }

  if (It == ActiveLayers.end()) {
    auto NewLayer = LayerFactory->CreateScreenLayer(weak_from_this(), RequestLayerId);
    ActiveLayers.insert(ActiveLayers.begin(), NewLayer);
  }
  else {
    ActiveLayers.erase(ActiveLayers.begin(), It);
  }

  RequestLayerId = IScreenLayerFactory::InvalidLayerId;
}