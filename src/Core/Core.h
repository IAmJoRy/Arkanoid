#pragma once

#include <cstdint>
#include <memory>

#include "Math.h"

class Renderer;
class ScreenLayer;
class ScreenManager;
