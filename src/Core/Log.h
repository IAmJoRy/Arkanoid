#pragma once

#include <iostream>
#include <sstream>
#include <string>

namespace Logger {

  struct detail {
    template <typename... Args>
    inline static void log(const char* modifier, Args... args) {
      std::cout << modifier;
      (std::cout << ... << args) << std::endl;
    }
  };

  template <typename... Args>
  inline void error(Args... args) {
    detail::log("[ERROR] ", args...);
  }

  template <typename... Args>
  inline void warning(Args... args) {
    detail::log("[WARNING] ", args...);
  }

  template <typename... Args>
  inline void info(Args... args) {
    detail::log("[INFO] ", args...);
  }
#ifdef _DEBUG
  template <typename... Args>
  inline void debug(Args... args) {
    detail::log("[DEBUG] ", args...);
  }
#else
  template <typename... Args>
  inline void debug(Args... args) {}
#endif

}  // namespace Logger