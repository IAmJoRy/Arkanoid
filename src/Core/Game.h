#pragma once

#include <chrono>
#include <string>

#include <SDL.h>

#include "Core/Renderer.h"
#include "Core/ScreenManager.h"

struct GameConfig {
  uint32_t WindowWidth;
  uint32_t WindowHeight;
  std::string_view WindowTitle;
};

class Game {
  public:
    Game() = default;

    bool Init(GameConfig& Config, std::unique_ptr<IScreenLayerFactory>&& LayerFactory);
    void Update();
    void Cleanup();

    bool IsRunning() const;

  private:
    void PollEvents();

  private:
    std::chrono::high_resolution_clock::time_point mTimeStamp;
    uint32_t WindowID = 0;
    bool mRunning = false;
    bool bHasFocus = false;
    SDL_Window* mWindow = nullptr;
    std::shared_ptr<Renderer> mRenderer;
    std::shared_ptr<ScreenManager> mManager;
};

