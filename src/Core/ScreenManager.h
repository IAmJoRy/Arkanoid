#pragma once

#include <memory>
#include <vector>

#include "SDL.h"

#include "Core/ScreenLayer.h"

class ScreenManager : public std::enable_shared_from_this<ScreenManager> {
  public:
    ScreenManager(std::unique_ptr<IScreenLayerFactory>&& InLayerFactory);

    void Update(float DeltaTime);
    void Draw(const std::shared_ptr<class Renderer>& Renderer) const;

    void KeyDownEvent(const FKeyboardEvent& Event);
    void KeyUpEvent(const FKeyboardEvent& Event);
    void WindowFocusInEvent();
    void WindowFocusOutEvent();

    void RequestTransition(const std::shared_ptr<ScreenLayer>& Requester, int32_t LayerId);

  private:
    void HandleTransitions();

  private:
    const std::unique_ptr<const IScreenLayerFactory> LayerFactory;
    int32_t RequestLayerId{ IScreenLayerFactory::InvalidLayerId };
    std::vector<std::shared_ptr<ScreenLayer>> ActiveLayers;

};