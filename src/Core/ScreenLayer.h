#pragma once

#include <unordered_map>
#include <memory>

#include "SDL.h"

#include "Core/Input.h"

class ScreenLayer : public IInputHandler, public std::enable_shared_from_this<ScreenLayer> {
  public:
    ScreenLayer(const std::weak_ptr<class ScreenManager>& InOwner) : mOwner(InOwner) {}
    virtual ~ScreenLayer() = default;

    virtual int32_t GetId() const = 0;

    virtual void Init() {}
    virtual void Update(float DeltaTime) {}
    virtual void Draw(const std::shared_ptr<class Renderer>& Renderer) const {}
    virtual void WindowFocusInEvent() {};
    virtual void WindowFocusOutEvent() {};

    virtual bool ShouldDrawPrevLayer() const { return true; }
    virtual bool ShouldUpdatePrevLayer() const { return false; }
    virtual bool ShouldHandleInputsPrevLayer() const { return false; }

    virtual bool HasBindings() const override;
    virtual InputDelegate GetBoundDelegate(EInputKey KeyCode) const override;

  protected:
    virtual void BindKey(EInputKey KeyCode, InputDelegate Func) override;

  protected:
    std::unordered_map<EInputKey, InputDelegate> mKeyBindings;
    std::weak_ptr<class ScreenManager> mOwner;
};

struct IScreenLayerFactory {
  public:
    IScreenLayerFactory(int32_t DefaultId = 0) : DefaulLayerId{ DefaultId } { }
    virtual ~IScreenLayerFactory() = default;

    virtual std::shared_ptr<ScreenLayer> CreateScreenLayer(const std::weak_ptr<class ScreenManager>& Owner, int32_t LayerID) const = 0;

    constexpr static int32_t InvalidLayerId = -1;
    const int32_t DefaulLayerId = 0;
};
