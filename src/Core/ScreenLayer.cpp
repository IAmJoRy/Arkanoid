#include "ScreenLayer.h"

#include "Core/Log.h"

bool ScreenLayer::HasBindings() const {
  return mKeyBindings.size() > 0;
}

void ScreenLayer::BindKey(EInputKey KeyCode, InputDelegate Func) {
  if (mKeyBindings.find(KeyCode) != mKeyBindings.end()) {
    mKeyBindings.erase(KeyCode);
    Logger::warning("ScreenLayer::BindKey: Rebinding key delegate for keycode", static_cast<uint32_t>(KeyCode));
  }
  mKeyBindings.insert(std::make_pair(KeyCode, std::move(Func)));
}

InputDelegate ScreenLayer::GetBoundDelegate(EInputKey KeyCode) const {
  if (auto it = mKeyBindings.find(KeyCode); it != mKeyBindings.end()) {
    return it->second;
  }
  return nullptr;
}