#pragma once

#include <cmath>
#include <cstdint>

namespace Math {

  //Generates random float 0-1
  inline float RandomFloat() {
    return (float)(std::rand()) / (float)(RAND_MAX);
  }

  //Generated random int32 in range [a, b)
  inline int32_t RandomInRange(int32_t a, int32_t b) {
    if (a > b) {
      return RandomInRange(b, a);
    }
    else if (a == b) {
      return a;
    }
    return a + (rand() % (b - a));
  }

}

struct FColor {
  uint8_t R = 0;
  uint8_t G = 0;
  uint8_t B = 0;
  uint8_t A = 0;
};

struct FPoint {
  float X = 0.0f;
  float Y = 0.0f;

  float DistSquared(const FPoint & Other) const {
    return std::pow(X - Other.X, 2.0f) + std::pow(Y - Other.Y, 2.0f);
  }
};


//2D Rectangle represented by top-left point and width + height
//mimics the SDL_FRect representation
struct FRect {
  float X = 0.0f;
  float Y = 0.0f;
  float Width = 0.0f;
  float Height = 0.0f;

  bool Intersects(const FRect& Other) const {
    return (X < (Other.X + Other.Width) && Y < (Other.Y + Other.Height) && (X + Width) > Other.X && (Y + Height) > Other.Y);
  }

  FPoint GetCenter() const { return FPoint{X + Width / 2.0f, Y + Height / 2.0f}; }

};

//AABB represented as center and half extent
struct FAABB {
  FPoint Center;
  FPoint Radius;

  FPoint GetMin() const { return { Center.X - Radius.X, Center.Y - Radius.Y }; }
  FPoint GetMax() const { return { Center.X + Radius.X, Center.Y + Radius.Y }; }

  bool Intersects(const FAABB& Other) const {
    FAABB MinkDiff = MinkowskiDifference(Other);
    FPoint Min = MinkDiff.GetMin();
    FPoint Max = MinkDiff.GetMax();
    return (Min.X < 0 && Max.X > 0 && Min.Y < 0 && Max.Y > 0);
  }

  FAABB MinkowskiDifference(const FAABB& Other) const {
    FAABB Result;
    Result.Center = { Center.X - Other.Center.X, Center.Y - Other.Center.Y };
    Result.Radius = { Radius.X + Other.Radius.X, Radius.Y + Other.Radius.Y };
    return Result;
  }

  FPoint PenetrationVector(const FAABB& Other) {
    FPoint Result;
    FAABB MinkDiff = MinkowskiDifference(Other);
    FPoint Min = MinkDiff.GetMin();
    FPoint Max = MinkDiff.GetMax();

    float MinDist = std::fabs(Min.X);
    Result.X = Min.X;
    Result.Y = 0;

    if (std::fabs(Max.X) < MinDist) {
      MinDist = std::fabs(Max.X);
      Result.X = Max.X;
    }

    if (std::fabs(Min.Y) < MinDist) {
      MinDist = std::fabs(Min.Y);
      Result.X = 0;
      Result.Y = Min.Y;
    }

    if (std::fabs(Max.Y) < MinDist) {
      Result.X = 0;
      Result.Y = Max.Y;
    }
    return Result;
  }
};