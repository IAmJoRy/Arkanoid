#pragma once

#include <string>
#include <map>

#include "Core/Math.h"

enum class EAlign {
  Left,
  Center,
  Right,
};

//Wrapper class around SDL_Renderer, calls SDL_DestroyRenderer in destructor
class Renderer {
  public:
    //Takes ownership of InRenderer
    Renderer(struct SDL_Renderer* InRenderer);
    ~Renderer();

    void Present();
    void Clear(const FColor& Color);
    void SetDrawColor(const FColor& Color);
    void DrawRect(const FRect& Rect);
    void DrawRectOutline(const FRect& Rect);
    void DrawCircle(const FPoint& Center, float Radius);

    bool SetFont(const std::string_view& FontName, const int32_t FontSize);
    void DrawText(const std::string_view& Text, const FPoint& Position, EAlign HorAlign = EAlign::Left, EAlign VerAlign = EAlign::Left);

    static void ClearFontCache();

  private:
    struct SDL_Renderer* SDLRender;
    struct _TTF_Font* mCurrentFont = nullptr;
    static std::map<std::string_view, struct _TTF_Font*> sFontCache;
};