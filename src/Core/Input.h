#pragma once

#include "SDL_keycode.h"

#include <functional>

enum class EInputKey {
  Unknown = SDLK_UNKNOWN,
  Return = SDLK_RETURN,
  Escape = SDLK_ESCAPE,
  Space = SDLK_SPACE,
  RightArrow = SDLK_RIGHT,
  LeftArrow = SDLK_LEFT,
};

enum class EInputState {
  Released,
  Pressed
};

struct FKeyboardEvent {
  EInputState State;
  bool IsRepeat;
  EInputKey Key;
};

using InputDelegate = std::function<void(EInputState)>;

class IInputHandler {
  public:
    IInputHandler() = default;
    virtual ~IInputHandler() = default;

    virtual bool HasBindings() const = 0;
    virtual InputDelegate GetBoundDelegate(EInputKey KeyCode) const = 0;

    virtual void BindKey(EInputKey KeyCode, InputDelegate Func) = 0;
};