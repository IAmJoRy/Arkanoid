#include "Game.h"

#include <chrono>
#include <cstdlib>

#include "SDL_ttf.h"

#include "Core/Log.h"

bool Game::Init(GameConfig& Config, std::unique_ptr<IScreenLayerFactory>&& LayerFactory) {
  if (SDL_Init(SDL_INIT_VIDEO) < 0) {
    Logger::error("SDL_Init: ", SDL_GetError());
    return false;
  }
  if (TTF_Init() != 0) {
    Logger::error("TTF_Init: ", SDL_GetError());
    return false;
  }
  mWindow = SDL_CreateWindow(Config.WindowTitle.data(), SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, Config.WindowWidth, Config.WindowHeight, 0);
  if (!mWindow) {
    Logger::error("SDL_CreateWindow: ", SDL_GetError());
    return false;
  }
  WindowID = SDL_GetWindowID(mWindow);
  auto SDLRender = SDL_CreateRenderer(mWindow, -1, SDL_RENDERER_ACCELERATED);
  if (!SDLRender) {
    Logger::error("SDL_CreateRenderer: ", SDL_GetError());
    return false;
  }
  SDL_SetRenderDrawBlendMode(SDLRender, SDL_BLENDMODE_BLEND);

  mRenderer = std::make_shared<Renderer>(SDLRender);
  mManager = std::make_shared<ScreenManager>(std::move(LayerFactory));
  mRunning = true;
  mTimeStamp = std::chrono::high_resolution_clock::now();
  return true;
}

bool Game::IsRunning() const {
  return mRunning;
}

void Game::Update() {
  constexpr int64_t TARGET_FPS = 120;
  constexpr int64_t TARGET_FRAME_US = std::micro().den / TARGET_FPS;
  
  const auto Now = std::chrono::high_resolution_clock::now();
  int64_t DeltaMicroseconds = std::chrono::duration_cast<std::chrono::microseconds>(Now - mTimeStamp).count();
  float DeltaSeconds = DeltaMicroseconds / static_cast<float>(std::micro().den);
  mTimeStamp = Now;

  PollEvents();
  mManager->Update(DeltaSeconds);
  mManager->Draw(mRenderer);
  mRenderer->Present();

  if (DeltaMicroseconds < TARGET_FRAME_US) {
    std::this_thread::sleep_for(std::chrono::microseconds(TARGET_FRAME_US - DeltaMicroseconds));
  }
}

void Game::PollEvents() {
  SDL_Event Event;
  while (SDL_PollEvent(&Event)) {
    switch (Event.type) {
    case SDL_KEYDOWN:
      if (WindowID == Event.key.windowID && bHasFocus) {
        mManager->KeyDownEvent({ EInputState::Pressed, Event.key.repeat > 0, static_cast<EInputKey>(Event.key.keysym.sym) });
      }
      break;
    case SDL_KEYUP:
      if (WindowID == Event.key.windowID && bHasFocus) {
        mManager->KeyUpEvent({ EInputState::Released, Event.key.repeat > 0, static_cast<EInputKey>(Event.key.keysym.sym) });
      }
      break;
    case SDL_QUIT:
      mRunning = false;
      return;
    case SDL_WINDOWEVENT:
      switch (Event.window.event) {
      case SDL_WINDOWEVENT_FOCUS_GAINED:
        if (Event.window.windowID == WindowID) {
          bHasFocus = true;
          mManager->WindowFocusInEvent();
        }
        break;
      case SDL_WINDOWEVENT_FOCUS_LOST:
        if (Event.window.windowID == WindowID) {
          bHasFocus = false;
          mManager->WindowFocusOutEvent();
        }
        break;
      }
      break;
    }
  }
}

void Game::Cleanup() {
  Renderer::ClearFontCache();
  SDL_DestroyWindow(mWindow);
  TTF_Quit();
  SDL_Quit();
}
