#include <cstdlib>

#include <Game/Arkanoid.h>

int main(int argc, char* args[]) {
  std::srand(std::time(0));
  ArkanoidGame Game;
  GameConfig Config{ Constants::cWindowWidth, Constants::cWindowHeight, Constants::cWindowTitle };

  if (!Game.Init(Config, std::make_unique<ArkanoidLayerFactory>())) {
    return EXIT_FAILURE;
  }
  while (Game.IsRunning()) {
    Game.Update();
  }
  Game.Cleanup();
  return EXIT_SUCCESS;
}