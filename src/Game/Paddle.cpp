#include "Game/Paddle.h"

#include "Core/Log.h"
#include "Core/Renderer.h"
#include "Game/Arkanoid.h"

Paddle::Paddle() : GameObject() {

}

void Paddle::Update(float DeltaTime) {
  const float MoveSpeed = static_cast<float>(mRightPressed - mLeftPressed) * 750.0f;
  const float MoveDelta = MoveSpeed * DeltaTime;
  SetOrigin({ mOrigin.X + MoveDelta, mOrigin.Y });
}

void Paddle::Draw(const std::shared_ptr<Renderer>& Renderer) const {
  auto Rect = GetRect();

  Renderer->SetDrawColor(FColor{ 120, 120, 120, 255 });
  Renderer->DrawRect(Rect);

  if (mGun) {
    Renderer->SetDrawColor(FColor{ 181, 68, 20, 255 });
  }
  else {
    Renderer->SetDrawColor(FColor{ 220, 220, 220, 255 });
  }
  Renderer->DrawRectOutline(Rect);
}

void Paddle::MoveRight(EInputState KeyEvent) {
  mRightPressed = KeyEvent == EInputState::Pressed;
}

void Paddle::MoveLeft(EInputState KeyEvent) {
  mLeftPressed = KeyEvent == EInputState::Pressed;
}