#pragma once

#include "Core/ScreenLayer.h"

class MainMenuLayer : public ScreenLayer {
  public:
    MainMenuLayer(const std::weak_ptr<class ScreenManager>& InOwner);
    virtual ~MainMenuLayer() = default;

    virtual void Init() override;
    virtual void Draw(const std::shared_ptr<class Renderer>& Renderer) const override;
    virtual int32_t GetId() const override;

  protected:
    void Start(EInputState KeyState);
};
