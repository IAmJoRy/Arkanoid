#include "GameLayer.h"

#include <cstdlib>

#include "Core/Log.h"
#include "Core/Renderer.h"
#include "Core/ScreenManager.h"
#include "Game/Arkanoid.h"
#include "Game/Fireball.h"
#include "Game/Levels.h"

GameLayer::GameLayer(const std::weak_ptr<class ScreenManager>& InOwner) : ScreenLayer(InOwner) {
}

int32_t GameLayer::GetId() const {
  return static_cast<int32_t>(EArkanoidLayer::Game);
}

void GameLayer::Init() {
  mLevelBounds = FRect{
    Constants::cBorderSize,
    Constants::cScoreboardOffset + Constants::cBorderSize,
    Constants::cWindowWidth - Constants::cBorderSize * 2,
    Constants::cWindowHeight - Constants::cScoreboardOffset
  };

  mPaddle = std::make_shared<Paddle>();
  mBall = std::make_shared<Ball>();
  mActiveLevel = 0;
  ArkanoidGame::sScore = 0;
  LoadLevel(mActiveLevel);
  ResetBallAndPaddle();
  mShowHelp = true;

  BindKey(EInputKey::LeftArrow, std::bind(&Paddle::MoveLeft, mPaddle.get(), std::placeholders::_1));
  BindKey(EInputKey::RightArrow, std::bind(&Paddle::MoveRight, mPaddle.get(), std::placeholders::_1));
  BindKey(EInputKey::Space, std::bind(&GameLayer::HitSpacebar, this, std::placeholders::_1));
  BindKey(EInputKey::Escape, std::bind(&GameLayer::Pause, this, std::placeholders::_1));
}

bool GameLayer::LoadLevel(int32_t Level) {
  mBricks.resize(Constants::cNumCol * Constants::cNumRow);

  if (Level < 0 || Level >= Constants::cNumLevels) {
    return false;
  }

  const float HalfBrickWidth = Constants::cBrickWidth / 2.0f;
  const float HalfBrickHeight = Constants::cBrickHeight / 2.0f;
  const float BeginX = mLevelBounds.X + HalfBrickWidth;
  const float BeginY = mLevelBounds.Y + Constants::cBrickHeight - HalfBrickHeight;


  NumAliveBricks = 0;
  for (int32_t Y = 0; Y < Constants::cNumRow; ++Y) {
    for (int32_t X = 0; X < Constants::cNumCol; ++X) {
      EBrickType Type = static_cast<EBrickType>(Constants::sLevelData[Level][Y][X]);
      mBricks[X + Y * Constants::cNumCol].reset();
      if (Type != EBrickType::None) {
        auto NewBrick = std::make_shared<Brick>(Type);
        NewBrick->SetOrigin({ BeginX + (X * Constants::cBrickWidth), BeginY + (Y * Constants::cBrickHeight) });
        NewBrick->SetSize({ Constants::cBrickWidth, Constants::cBrickHeight });
        mBricks[X + Y * Constants::cNumCol] = std::move(NewBrick);
        if (Type != EBrickType::Gold) {
          NumAliveBricks++;
        }
      }
    }
  }

  return true;
}

void GameLayer::Update(float DeltaTime) {
  mPaddle->Update(DeltaTime);
  ConstrainInbound(mPaddle);

  mBall->Update(DeltaTime);
  auto BallCollision = ConstrainInbound(mBall);
  if (BallCollision & ECollisionSideFlags::Left || BallCollision & ECollisionSideFlags::Right) {
    FPoint BallDirection = mBall->GetDirection();
    mBall->SetDirection({ -BallDirection.X, BallDirection.Y });
  }
  if (BallCollision & ECollisionSideFlags::Top ) {
    FPoint BallDirection = mBall->GetDirection();
    mBall->SetDirection({ BallDirection.X, -BallDirection.Y});
  }
  else if (BallCollision & ECollisionSideFlags::Bottom) {
    mBonus.reset();
    mLives--;
    if (mLives > 0) {
      ResetBallAndPaddle();
    }
    else {
      mOwner.lock()->RequestTransition(shared_from_this(), static_cast<int32_t>(EArkanoidLayer::MainMenu));
    }
    return;
  }

  if (mBonus) {
    mBonus->Update(DeltaTime);
    if (mBonus->GetOrigin().Y > mLevelBounds.Y + mLevelBounds.Height) {
      mBonus.reset();
    }
  }

  ProcessCollisions(DeltaTime);

  if (NumAliveBricks == 0) {
    mActiveLevel++;
    if (LoadLevel(mActiveLevel)) {
      mBonus.reset();
      mProjectiles.clear();
      ResetBallAndPaddle();
    }
    else {
      mOwner.lock()->RequestTransition(shared_from_this(), static_cast<int32_t>(EArkanoidLayer::MainMenu));
    }
  }
}

//Constrains the input object inside the level bounds and returns which side the level boundary
//the object collided with if any
GameLayer::ECollisionSideFlags GameLayer::ConstrainInbound(const std::shared_ptr<GameObject>& Object) {
  int8_t Flags = GameLayer::ECollisionSideFlags::None;
  
  auto ObjRect = Object->GetRect();
  const auto& ObjOrigin = Object->GetOrigin();
  auto ObjHalfSize = FPoint(ObjRect.Width / 2.0f, ObjRect.Height / 2.0f);

  if (ObjRect.X < mLevelBounds.X) {
    Object->SetOrigin({ mLevelBounds.X + ObjHalfSize.X, ObjOrigin.Y });
    Flags |= ECollisionSideFlags::Left;
  }
  else if (ObjRect.X + ObjRect.Width > mLevelBounds.X + mLevelBounds.Width) {
    Object->SetOrigin({ mLevelBounds.X + mLevelBounds.Width - ObjHalfSize.X, ObjOrigin.Y });
    Flags |= ECollisionSideFlags::Right;
  }

  if (ObjRect.Y < mLevelBounds.Y) {
    Object->SetOrigin({ ObjOrigin.X, mLevelBounds.Y + ObjHalfSize.Y});
    Flags |= ECollisionSideFlags::Top;
  }
  else if (ObjRect.Y + ObjRect.Height > mLevelBounds.Y + mLevelBounds.Height) {
    Object->SetOrigin({ ObjOrigin.X, mLevelBounds.Y + mLevelBounds.Height - ObjHalfSize.Y });
    Flags |= ECollisionSideFlags::Bottom;
  }

  return static_cast<GameLayer::ECollisionSideFlags>(Flags);
}

void GameLayer::ProcessCollisions(float DeltaTime) {
  for (auto It = mProjectiles.begin(); It != mProjectiles.end();) {
    (*It)->Update(DeltaTime);
    auto CollidedBrick = GetCollidedBrick(*It);
    if (CollidedBrick && CollidedBrick->GetType() != EBrickType::Gold) {
      DamageBrick(CollidedBrick, *It);
      It = mProjectiles.erase(It);
      continue;
    }
    if ((*It)->GetOrigin().Y < mLevelBounds.Y) {
      It = mProjectiles.erase(It);
      continue;
    }
    It++;
  }

  auto BallAABB = mBall->GetAABB();
  if (BallAABB.Intersects(mPaddle->GetAABB())) {
    mBall->OnCollide(mPaddle);
    mPaddle->OnCollide(mBall);
  }

  if (auto CollidedBrick = GetCollidedBrick(mBall); CollidedBrick != nullptr) {
    mBall->OnCollide(CollidedBrick);
    DamageBrick(CollidedBrick, mBall);
  }

  if (mBonus && mBonus->GetAABB().Intersects(mPaddle->GetAABB())) {
    ActivateBonus(mBonus->GetType());
    mBonus.reset();
  }
}

std::shared_ptr<Brick> GameLayer::GetCollidedBrick(const std::shared_ptr<GameObject>& Object) {
  std::shared_ptr<Brick> CollidedBrick;
  float MinDistance = std::numeric_limits<float>::max();
  auto ObjectAABB = Object->GetAABB();
  for (auto& It : mBricks) {
    if (!It || It->GetHealth() <= 0) {
      continue;
    }
    FAABB MinkDiff = ObjectAABB.MinkowskiDifference(It->GetAABB());
    FPoint Min = MinkDiff.GetMin();
    FPoint Max = MinkDiff.GetMax();
    if (Min.X > 0 || Max.X < 0 || Min.Y > 0 || Max.Y < 0) {
      continue;
    }
    float Distance = ObjectAABB.Center.DistSquared(It->GetOrigin());
    if (Distance < MinDistance) {
      CollidedBrick = It;
      MinDistance = Distance;
    }
  }
  return CollidedBrick;
}

void GameLayer::DamageBrick(const std::shared_ptr<Brick>& Brick, const std::shared_ptr<GameObject>& Damager) {
  Brick->OnCollide(Damager);
  if (Brick->GetHealth() <= 0) {
    ArkanoidGame::sScore += Brick->GetScore();
    NumAliveBricks--;

    //Randomly spawn bonus pickup if there is none
    if (!mBonus && Math::RandomFloat() < 0.1f) {
      EBonusType RandType = static_cast<EBonusType>(Math::RandomInRange(static_cast<int32_t>(EBonusType::NONE) + 1, static_cast<int32_t>(EBonusType::MAX)));
      mBonus = std::make_shared<Bonus>(RandType);
      mBonus->SetOrigin(Brick->GetOrigin());
    }
  }
}

void GameLayer::Draw(const std::shared_ptr<class Renderer>& Renderer) const {
  Renderer->Clear(FColor{ 20, 20, 20, 255 });

  Renderer->SetDrawColor(FColor{255, 255, 255, 255});
  Renderer->SetFont(Constants::cFont, 16);
  std::string ScoreStr = "Score: " + std::to_string(ArkanoidGame::sScore);
  Renderer->DrawText(ScoreStr, {Constants::cBorderSize, Constants::cBorderSize / 2.0f });

  std::string LivesStr = "Lives: " + std::to_string(mLives);
  Renderer->DrawText(LivesStr, { Constants::cWindowWidth - Constants::cBorderSize, Constants::cBorderSize / 2.0f }, EAlign::Right);

  std::string LevelStr = "Level: " + std::to_string(mActiveLevel + 1);
  Renderer->DrawText(LevelStr, { Constants::cWindowWidth / 2.0f, Constants::cBorderSize / 2.0f }, EAlign::Center);

  for (const auto& It : mBricks) {
    if (It && It->GetHealth() > 0) {
      It->Draw(Renderer);
    }
  }
  for (const auto& It : mProjectiles) {
    It->Draw(Renderer);
  }
  if(mBonus) {
    mBonus->Draw(Renderer);
  }

  //LevelBounds
  Renderer->SetDrawColor(FColor{ 150, 150, 150, 255});
  Renderer->DrawRect({0.0f, mLevelBounds.Y, mLevelBounds.X, mLevelBounds.Y + mLevelBounds.Height});
  Renderer->DrawRect({ Constants::cWindowWidth - Constants::cBorderSize, mLevelBounds.Y, Constants::cBorderSize, mLevelBounds.Height });
  Renderer->DrawRect({ 0.0f, mLevelBounds.Y - Constants::cBorderSize, Constants::cWindowWidth, Constants::cBorderSize});

  mPaddle->Draw(Renderer);
  mBall->Draw(Renderer);

  if (mShowHelp) {
    Renderer->SetDrawColor(FColor{ 255, 255, 255, 255 });
    Renderer->DrawText("Use ARROW KEYS to move and SPACE to release", {Constants::cWindowWidth / 2.0f, Constants::cWindowHeight / 2.0f}, EAlign::Center);
  }
}

void GameLayer::HitSpacebar(EInputState KeyEvent) {
  if (KeyEvent == EInputState::Released) {
    if (mBall->IsAttached()) {
      mBall->AttachTo(std::weak_ptr<GameObject>{});
      mShowHelp = false;
    }
    else if(mPaddle->IsGunEnabled()) {
      auto Tick = SDL_GetTicks();
      if (Tick - mLastShootTick < 200) {
        return;
      }
      auto& NewProjectile = mProjectiles.emplace_back(new Fireball());
      NewProjectile->SetOrigin(mPaddle->GetOrigin());
      mLastShootTick = Tick;
    }
  }
}

void GameLayer::ResetBallAndPaddle() {
  ActivateBonus(EBonusType::NONE);
  mPaddle->SetOrigin({ mLevelBounds.GetCenter().X, Constants::cWindowHeight - mPaddle->GetSize().Y * 2.5f });
  mBall->SetDirection({0.0f, -1.0f });
  mBall->AttachTo(mPaddle);
}

void GameLayer::Pause(EInputState KeyEvent) {
  if (KeyEvent == EInputState::Released) {
    mOwner.lock()->RequestTransition(shared_from_this(), static_cast<int32_t>(EArkanoidLayer::Pause));
  }
}

void GameLayer::WindowFocusOutEvent() {
  mOwner.lock()->RequestTransition(shared_from_this(), static_cast<int32_t>(EArkanoidLayer::Pause));
}

void GameLayer::ActivateBonus(EBonusType Type) {
  if (Type == EBonusType::ExtraLife) {
    mLives++;
    return;
  }
  if (Type == EBonusType::LongPlatform) {
    mPaddle->SetSize({ Constants::cBrickWidth * 2.0f, Constants::cBrickHeight });
  }
  else {
    mPaddle->SetSize({ Constants::cBrickWidth * 1.5f, Constants::cBrickHeight });
  }
  mPaddle->SetGunEnabled(Type == EBonusType::Gun );
}