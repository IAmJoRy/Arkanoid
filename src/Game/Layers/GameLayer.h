#pragma once

#include <array>

#include "Core/ScreenLayer.h"
#include "Game/Arkanoid.h"
#include "Game/Ball.h"
#include "Game/Bonus.h"
#include "Game/Brick.h"
#include "Game/Paddle.h"

class GameLayer : public ScreenLayer {
  public:
    GameLayer(const std::weak_ptr<class ScreenManager>& InOwner);
    virtual ~GameLayer() = default;

    virtual void Init() override;
    virtual void Update(float DeltaTime) override;
    virtual void Draw(const std::shared_ptr<class Renderer>& Renderer) const override;
    virtual bool ShouldDrawPrevLayer() const override { return false; }
    virtual void WindowFocusOutEvent() override;

    virtual int32_t GetId() const override;

  protected:
    enum ECollisionSideFlags : int8_t {
      None   = 0x0,
      Left   = 0x1,
      Right  = 0x2,
      Top    = 0x4,
      Bottom = 0x8
    };
    ECollisionSideFlags ConstrainInbound(const std::shared_ptr<GameObject>& Object);
    std::shared_ptr<Brick> GetCollidedBrick(const std::shared_ptr<GameObject>& Object);
    void DamageBrick(const std::shared_ptr<Brick>& Brick, const std::shared_ptr<GameObject>& Damager);
    void HitSpacebar(EInputState KeyEvent);
    void Pause(EInputState KeyEvent);
    void ResetBallAndPaddle();
    void ProcessCollisions(float DeltaTime);
    void ActivateBonus(EBonusType Type);
    bool LoadLevel(int32_t Level);
  
  protected:
    std::shared_ptr<Paddle> mPaddle;
    std::shared_ptr<Ball> mBall;
    std::shared_ptr<Bonus> mBonus;
    std::vector<std::shared_ptr<Brick>> mBricks;
    std::list<std::shared_ptr<GameObject>> mProjectiles;
    FRect mLevelBounds;
    int32_t mActiveLevel = 0;
    int32_t NumAliveBricks = 0;
    int32_t mLives = 3;
    uint32_t mLastShootTick = 0;
    bool mShowHelp = false;
};
