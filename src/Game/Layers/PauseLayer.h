#pragma once
#pragma once

#include "Core/ScreenLayer.h"

class PauseLayer : public ScreenLayer {
public:
  PauseLayer(const std::weak_ptr<class ScreenManager>& InOwner);
  virtual ~PauseLayer() = default;

  virtual void Init() override;
  virtual void Draw(const std::shared_ptr<class Renderer>& Renderer) const override;
  virtual int32_t GetId() const override;

protected:
  void End(EInputState KeyState);
  void Continue(EInputState KeyState);
};
