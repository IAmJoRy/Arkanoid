#include "MainMenuLayer.h"

#include "Core/Renderer.h"
#include "Core/ScreenManager.h"
#include "Game/Arkanoid.h"

MainMenuLayer::MainMenuLayer(const std::weak_ptr<class ScreenManager>& InOwner) : ScreenLayer(InOwner) {
}

int32_t MainMenuLayer::GetId() const {
  return static_cast<int32_t>(EArkanoidLayer::MainMenu);
}

void MainMenuLayer::Init() {
  BindKey(EInputKey::Return, std::bind(&MainMenuLayer::Start, this, std::placeholders::_1));
}

void MainMenuLayer::Start(EInputState KeyState) {
  if (KeyState == EInputState::Released && !mOwner.expired()) {
    mOwner.lock()->RequestTransition(shared_from_this(), static_cast<int32_t>(EArkanoidLayer::Game));
  }
}

void MainMenuLayer::Draw(const std::shared_ptr<class Renderer>& Renderer) const {
  Renderer->Clear(FColor{20, 20, 20, 255});

  FPoint TextPosition = { Constants::cWindowWidth * 0.5f, Constants::cWindowHeight * 0.4f };

  Renderer->SetDrawColor(FColor{ 255, 255, 255, 255 });
  Renderer->SetFont(Constants::cFont, 60);
  Renderer->DrawText(Constants::cWindowTitle, TextPosition, EAlign::Center, EAlign::Center);

  Renderer->SetFont(Constants::cFont, 16);
  Renderer->DrawText("Press RETURN to play", { TextPosition.X, TextPosition.Y + 80.0f }, EAlign::Center, EAlign::Center);

  if (ArkanoidGame::sScore > 0) {
    std::string LastScoreString = "Last Score: " + std::to_string(ArkanoidGame::sScore);
    Renderer->DrawText(LastScoreString, { TextPosition.X, TextPosition.Y + 160.0f }, EAlign::Center, EAlign::Center);
  }
}