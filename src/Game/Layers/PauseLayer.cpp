#include "PauseLayer.h"

#include "Core/Renderer.h"
#include "Game/Arkanoid.h"

PauseLayer::PauseLayer(const std::weak_ptr<class ScreenManager>& InOwner) : ScreenLayer(InOwner) {
}

int32_t PauseLayer::GetId() const {
  return static_cast<int32_t>(EArkanoidLayer::Pause);
}

void PauseLayer::Init() {
  BindKey(EInputKey::Return, std::bind(&PauseLayer::Continue, this, std::placeholders::_1));
  BindKey(EInputKey::Escape, std::bind(&PauseLayer::End, this, std::placeholders::_1));
}

void PauseLayer::Draw(const std::shared_ptr<class Renderer>& Renderer) const {
  Renderer->SetDrawColor({ 20, 20, 20, 190 });
  Renderer->DrawRect({0, 0, Constants::cWindowWidth, Constants::cWindowHeight });

  FPoint TextPosition = { Constants::cWindowWidth * 0.5f, Constants::cWindowHeight * 0.4f };

  Renderer->SetDrawColor(FColor{ 255, 255, 255, 255 });
  Renderer->SetFont(Constants::cFont, 60);
  Renderer->DrawText("PAUSED", TextPosition, EAlign::Center, EAlign::Center);

  Renderer->SetFont(Constants::cFont, 16);
  Renderer->DrawText("Press RETURN to continue or ESC to exit", { TextPosition.X, TextPosition.Y + 120.0f }, EAlign::Center, EAlign::Center);
}

void PauseLayer::End(EInputState KeyState) {
  if (KeyState == EInputState::Released) {
    mOwner.lock()->RequestTransition(shared_from_this(), static_cast<int32_t>(EArkanoidLayer::MainMenu));
  }
}

void PauseLayer::Continue(EInputState KeyState) {
  if (KeyState == EInputState::Released) {
    mOwner.lock()->RequestTransition(shared_from_this(), static_cast<int32_t>(EArkanoidLayer::Game));
  }
}