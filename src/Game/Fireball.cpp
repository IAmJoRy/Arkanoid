#include "Fireball.h"

#include "Core/Renderer.h"

Fireball::Fireball() : GameObject() {
  SetSize({ 8.0f, 8.0f });
}

void Fireball::Update(float DeltaTime) {
  SetOrigin({ mOrigin.X, mOrigin.Y - 500.0f * DeltaTime });
}

void Fireball::Draw(const std::shared_ptr<Renderer>& Renderer) const {
  Renderer->SetDrawColor(FColor{ 181, 68, 20, 255 });
  Renderer->DrawCircle(mOrigin, mSize.X * 0.5f);
}
