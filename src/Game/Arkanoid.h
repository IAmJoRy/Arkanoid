#pragma once

#include <string>

#include "Core/Game.h"
#include "Core/ScreenLayer.h"

namespace Constants {
  inline static constexpr int32_t cWindowWidth = 672;
  inline static constexpr int32_t cWindowHeight = 768;
  inline static constexpr std::string_view cWindowTitle = "Wish.com Arkanoid";

  inline static constexpr int32_t cNumRow = 18;
  inline static constexpr int32_t cNumCol = 13;
  inline static constexpr float cBorderSize = 24.0f;
  inline static constexpr float cScoreboardOffset = 48.0f;

  inline static constexpr float cBrickWidth = (cWindowWidth - cBorderSize * 2) / cNumCol;
  inline static constexpr float cBrickHeight = cBrickWidth / 2.0f;

  inline static constexpr std::string_view cFont = "rsc/8bit.ttf";
};

enum class EArkanoidLayer {
  Undefined = 0,
  MainMenu,
  Game,
  Pause,
};

struct ArkanoidLayerFactory : public IScreenLayerFactory {
  ArkanoidLayerFactory() : IScreenLayerFactory(static_cast<int32_t>(EArkanoidLayer::MainMenu)) { }
  virtual std::shared_ptr<ScreenLayer> CreateScreenLayer(const std::weak_ptr<class ScreenManager>& Owner, int32_t LayerID) const override;
};

class ArkanoidGame : public Game {
  public:
    ArkanoidGame() = default;

    static int32_t sScore;
};