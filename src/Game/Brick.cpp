#include "Brick.h"

#include "Core/Renderer.h"

const std::array<Brick::FBrickData, static_cast<size_t>(EBrickType::MAX)> Brick::sBrickData = {
  FBrickData{},//NONE
  FBrickData{ 1, 50, {241, 241, 241, 255} },//White,
  FBrickData{ 1, 60, {255, 143, 0, 255} },//Orange,
  FBrickData{ 1, 70, {0, 255, 255, 255} },//Cyan,
  FBrickData{ 1, 80, {0, 255, 0, 255} },//Green,
  FBrickData{ 1, 90, {255, 0, 0, 255} },//Red,
  FBrickData{ 1, 100, {0, 112, 255, 255} },//Blue,
  FBrickData{ 1, 110, {255, 0, 255, 255} },//Pink,
  FBrickData{ 1, 120, {255, 255, 0, 255} },//Yellow,
  FBrickData{ 2, 50, {157, 157, 157, 255}, {210, 210, 210, 255}, {125, 125, 125, 255} },//Silver,
  FBrickData{ 1, 0, {188, 174, 0, 255}, {224, 210, 0, 255}, {157, 143, 0, 255} },//Gold,
};


Brick::Brick(EBrickType Type) : GameObject() {
  mType = Type;
  mHealth = GetBlockData().Health;
}

int32_t Brick::GetScore() const {
  return GetBlockData().Score;
}

int32_t Brick::GetHealth() const {
  return mHealth;
}

EBrickType Brick::GetType() const {
  return mType;
}

const Brick::FBrickData& Brick::GetBlockData() const {
  return sBrickData[static_cast<size_t>(mType)];
}

void Brick::Draw(const std::shared_ptr<Renderer>& Renderer) const {
  FRect Rect = GetRect();
  float OutlineSize = Rect.Height * 0.125f;
  Renderer->SetDrawColor(GetBlockData().Background);
  Renderer->DrawRect(Rect);

  if (mType == EBrickType::Silver || mType == EBrickType::Gold) {
    Renderer->SetDrawColor(GetBlockData().Highlight);
    Renderer->DrawRect({ Rect.X, Rect.Y, Rect.Width - OutlineSize, OutlineSize });
    Renderer->DrawRect({ Rect.X, Rect.Y, OutlineSize, Rect.Height - OutlineSize });

    Renderer->SetDrawColor(GetBlockData().Shadow);
    Renderer->DrawRect({ Rect.X, Rect.Y + Rect.Height - OutlineSize * 2.0f, Rect.Width, OutlineSize });
    Renderer->DrawRect({ Rect.X + Rect.Width - OutlineSize * 2.0f, Rect.Y, OutlineSize, Rect.Height });
  }

  Renderer->SetDrawColor({0, 0, 0, 255});
  Renderer->DrawRect({Rect.X, Rect.Y + Rect.Height - OutlineSize, Rect.Width, OutlineSize });
  Renderer->DrawRect({Rect.X + Rect.Width - OutlineSize, Rect.Y, OutlineSize, Rect.Height });
}

void Brick::OnCollide(const std::shared_ptr<GameObject>& Other) {
  if (mHealth > 0 && mType != EBrickType::Gold) {
    mHealth--;
  }
}