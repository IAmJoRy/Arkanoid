#pragma once

#include "Core/Input.h"
#include "Game/Object.h"

class Paddle : public GameObject {
  public:
    Paddle();
    virtual ~Paddle() = default;

    virtual void Update(float DeltaTime) override;
    virtual void Draw(const std::shared_ptr<Renderer>& Renderer) const override;

    void MoveLeft(EInputState KeyEvent);
    void MoveRight(EInputState KeyEvent);
    void SetGunEnabled(bool Enabled) { mGun = Enabled; }
    bool IsGunEnabled() const { return mGun; }

  private:
    bool mLeftPressed = false;
    bool mRightPressed = false;
    bool mGun = false;
};