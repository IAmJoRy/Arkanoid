#pragma once

#include <array>

#include "Game/Object.h"

enum class EBrickType {
  None = 0,
  White,
  Orange,
  Cyan,
  Green,
  Red,
  Blue,
  Pink,
  Yellow,
  Silver,
  Gold,
  MAX,
};

class Brick : public GameObject {
  public:
    Brick(EBrickType Type);
    virtual ~Brick() = default;

    virtual void Draw(const std::shared_ptr<Renderer>& Renderer) const override;
    virtual void OnCollide(const std::shared_ptr<GameObject>& Other) override;

    int32_t GetScore() const;
    int32_t GetHealth() const;
    EBrickType GetType() const;

  private:
    int32_t mHealth;
    EBrickType mType;

  private:
    struct FBrickData {
      int32_t Health;
      int32_t Score;
      FColor Background;
      FColor Highlight;
      FColor Shadow;
    };
    
    const FBrickData& GetBlockData() const;

    const static std::array<FBrickData, static_cast<size_t>(EBrickType::MAX)> sBrickData;
};