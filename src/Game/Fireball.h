#pragma once

#include "Game/Object.h"

class Fireball : public GameObject {
public:
  Fireball();
  virtual ~Fireball() = default;

  virtual void Update(float DeltaTime) override;
  virtual void Draw(const std::shared_ptr<Renderer>& Renderer) const override;
};