#include "Bonus.h"

#include "Game/Arkanoid.h"

const std::array<Bonus::EBonusRenderData, static_cast<size_t>(EBonusType::MAX)> Bonus::sBonusData = {
  EBonusRenderData{}, //None
  EBonusRenderData{ "E", {20, 181, 40, 255} }, //ExtraLife
  EBonusRenderData{ "L", {20, 58, 181, 255} }, //LongPlatform
  EBonusRenderData{ "G", {181, 68, 20, 255} }, //Gun
};

Bonus::Bonus(EBonusType Type) : GameObject(), mType(Type) {
  SetSize({ Constants::cBrickWidth * 0.75f, Constants::cBrickHeight * 0.75f });
}

void Bonus::Update(float DeltaTime) {
  SetOrigin({ mOrigin.X, mOrigin.Y + 75.0f * DeltaTime });
}

const Bonus::EBonusRenderData& Bonus::GetBonusData() const {
  return sBonusData[static_cast<size_t>(mType)];
}

void Bonus::Draw(const std::shared_ptr<Renderer>& Renderer) const {
  auto Rect = GetRect();
  const auto& RenderData = GetBonusData();

  Renderer->SetDrawColor(RenderData.Background);
  Renderer->DrawRect(Rect);

  Renderer->SetDrawColor({ 255, 255, 255, 255 });
  Renderer->SetFont(Constants::cFont, 16);
  Renderer->DrawText(RenderData.Text, mOrigin, EAlign::Center, EAlign::Center);
  Renderer->DrawRectOutline(Rect);
}
