#pragma once

#include <array>
#include <string_view>

#include "Game/Object.h"

enum class EBonusType : uint8_t {
  NONE,
  ExtraLife,
  LongPlatform,
  Gun,

  MAX
};

class Bonus : public GameObject {
  public:
    Bonus(EBonusType Type);
    virtual ~Bonus() = default;

    virtual void Update(float DeltaTime) override;
    virtual void Draw(const std::shared_ptr<Renderer>& Renderer) const override;

    EBonusType GetType() const { return mType; }

  private:
    EBonusType mType;
  private:
    struct EBonusRenderData {
      std::string_view Text;
      FColor Background;
    };

    const EBonusRenderData& GetBonusData() const;
    const static std::array<EBonusRenderData, static_cast<size_t>(EBonusType::MAX)> sBonusData;
};