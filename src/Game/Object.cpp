#include "Object.h"

void GameObject::SetOrigin(const FPoint& Origin) {
  mOrigin = Origin;
}

const FPoint& GameObject::GetOrigin() const {
  return mOrigin;
}

void GameObject::SetSize(const FPoint& Size) {
  mSize = Size;
}

const FPoint& GameObject::GetSize() const {
  return mSize;
}

FRect GameObject::GetRect() const {
  float HalfX = mSize.X / 2.0f;
  float HalfY = mSize.Y / 2.0f;
  return FRect{ mOrigin.X - HalfX, mOrigin.Y - HalfY, mSize.X, mSize.Y };
}

FAABB GameObject::GetAABB() const {
  return FAABB{ mOrigin, {mSize.X / 2.0f, mSize.Y / 2.0f} };
}

bool GameObject::IsPendingKill() const {
  return mPendingKill;
}

void GameObject::MarkPendingKill() {
  mPendingKill = true;
}