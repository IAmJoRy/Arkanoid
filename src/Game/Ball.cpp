#include "Ball.h"

#include "Core/Log.h"
#include "Core/Renderer.h"
#include "Game/Paddle.h"

Ball::Ball() : GameObject() {
  SetSize({ 8.0f, 8.0f });
}

void Ball::Update(float DeltaTime) {
  if (!IsAttached()) {
    SetOrigin({ mOrigin.X + mDirection.X * mSpeed * DeltaTime, mOrigin.Y + mDirection.Y * mSpeed * DeltaTime });
  }
  else {
    FRect ParentRect = mAttachedTo.lock()->GetRect();
    SetOrigin({ ParentRect.X + ParentRect.Width / 2.0f, ParentRect.Y - mSize.Y});
  }
}

void Ball::Draw(const std::shared_ptr<Renderer>& Renderer) const {
  Renderer->SetDrawColor(FColor{ 240, 240, 240, 255 });
  Renderer->DrawCircle(mOrigin, mSize.X * 0.5f);
}

void Ball::OnCollide(const std::shared_ptr<GameObject>& Other) {
  if (IsAttached()) {
    return;
  }
  
  auto Diff = GetAABB().PenetrationVector(Other->GetAABB());
  SetOrigin({ mOrigin.X - Diff.X, mOrigin.Y - Diff.Y });

  if (std::dynamic_pointer_cast<Paddle>(Other)) {
    mDirection = { (mOrigin.X - Other->GetOrigin().X) / Other->GetSize().X * 2.0f,  -1.0f };
    const float Lenght = std::sqrt(mDirection.X * mDirection.X + mDirection.Y * mDirection.Y);
    mDirection.X /= Lenght;
    mDirection.Y /= Lenght;
  }
  else {
    if (std::fabs(Diff.X) < std::fabs(Diff.Y)) {
      mDirection.Y = -mDirection.Y;
    }
    else {
      mDirection.X = -mDirection.X;
    }
  }
}

bool Ball::IsAttached() const {
  return !mAttachedTo.expired();
}

void Ball::AttachTo(const std::weak_ptr<GameObject>& Object) {
  mAttachedTo = Object;
}

const FPoint& Ball::GetDirection() const {
  return mDirection;
}

void Ball::SetDirection(const FPoint& Direction) {
  mDirection = Direction;
}