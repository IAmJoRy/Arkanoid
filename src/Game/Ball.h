#pragma once

#include "Core/Input.h"
#include "Game/Object.h"

class Ball : public GameObject {
  public:
    Ball();
    virtual ~Ball() = default;

    virtual void Update(float DeltaTime) override;
    virtual void Draw(const std::shared_ptr<Renderer>& Renderer) const override;
    virtual void OnCollide(const std::shared_ptr<GameObject>& Other) override;

    bool IsAttached() const;
    void AttachTo(const std::weak_ptr<GameObject>& Object);
    const FPoint& GetDirection() const;
    void SetDirection(const FPoint& Direction);

  private:
    FPoint mDirection{ 0.0f, 0.0f };
    float mSpeed = 500.0f;
    std::weak_ptr<GameObject> mAttachedTo;
};