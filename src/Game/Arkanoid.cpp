#include "Arkanoid.h"

#include "Game/Layers/MainMenuLayer.h"
#include "Game/Layers/GameLayer.h"
#include "Game/Layers/PauseLayer.h"

int32_t ArkanoidGame::sScore;

std::shared_ptr<ScreenLayer> ArkanoidLayerFactory::CreateScreenLayer(const std::weak_ptr<class ScreenManager>& Owner, int32_t LayerID) const {
  std::shared_ptr<ScreenLayer> Layer = nullptr;
  switch (static_cast<EArkanoidLayer>(LayerID)) {
    case EArkanoidLayer::MainMenu:
      Layer = std::make_shared<MainMenuLayer>(Owner);
      break;
    case EArkanoidLayer::Game:
      Layer = std::make_shared<GameLayer>(Owner);
      break;
    case EArkanoidLayer::Pause:
      Layer = std::make_shared<PauseLayer>(Owner);
      break;
  }
  if (Layer) {
    Layer->Init();
  }
  return Layer;
}