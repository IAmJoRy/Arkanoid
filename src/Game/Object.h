#pragma once

#include "Core/Core.h"

class GameObject : public std::enable_shared_from_this<GameObject> {
  public:
    GameObject() = default;
    virtual ~GameObject() = default;

    void SetOrigin(const FPoint& Origin);
    const FPoint& GetOrigin() const;
    void SetSize(const FPoint& Size);
    const FPoint& GetSize() const;
    FRect GetRect() const;
    FAABB GetAABB() const;
    void MarkPendingKill();
    bool IsPendingKill() const;

    virtual void Update(float DeltaTime) {}
    virtual void Draw(const std::shared_ptr<Renderer>& Renderer) const {}
    virtual void OnCollide(const std::shared_ptr<GameObject>& Other) {}

  protected:
    FPoint mOrigin{};
    FPoint mSize{};
    bool mPendingKill = false;
};